import cv2;
from bottlesrecognizer import BottlesRecognizer
from colorama import init, Fore
from imagedisplayer import ImageDisplayer

init();

expectedNumberOfBottles = [
    20, 18, 20, 20, 20, 20, 20, 20, 13, 18, 20, 20, 19, 20, 19, 20, 17, 20, 16, 19, 11, 11, 20, 20
];

pathsToImages = [];

def getNumberAsString(x):
    return str(x) if x > 9 else "0" + str(x)

def generatePathsToImages():
    prefix = 'img/bottle_crate_';
    suffix = '.png';
    scope = [1, 25];
    for x in range(scope[0], scope[1]):
        pathsToImages.append(prefix + getNumberAsString(x) + suffix);

def getColor(assertionResult):
    return Fore.GREEN if assertionResult else Fore.RED;

def check(expected, actual):
    return expected == actual;

def printSummary(result, total):
    print(Fore.BLUE + 'Passed ' + str(result) + ' tests, ' + str(total) + ' tests executed in total.');

def printAssertionResult(path, color, expected, actual):
    print(color + path + ': Expected: ' + str(expected) + ' Actual: ' + str(actual));

def assertion(path, expected, actual):
    result = check(expected,actual);
    color = getColor(result);
    printAssertionResult(path, color, expected, actual);
    return result;

def display(img):
    imageDisplayer = ImageDisplayer();
    imageDisplayer.display(img, '');

def main():
    generatePathsToImages();
    testData = list(zip(pathsToImages, expectedNumberOfBottles));
    amountOfPassedTests = 0;

    for path,expected in testData:
        bottlesRecognizer = BottlesRecognizer(path);
        actual = bottlesRecognizer.countBotttles();
        if assertion(path, expected, actual):
            amountOfPassedTests += 1;
        img = bottlesRecognizer.getImageWithMarkedBottles();
        display(img);

    amountOfTests = len(testData);
    printSummary(amountOfPassedTests, amountOfTests);


if __name__ == "__main__":
    main();
