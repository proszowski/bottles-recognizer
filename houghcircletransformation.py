import cv2
import numpy as np

class HoughCircleTransformation:
    def __init__(self, img):
        self.img = img;
        self.circles = cv2.HoughCircles(self.img,cv2.HOUGH_GRADIENT,1,20, param1=260,param2=30,minRadius=14,maxRadius=45);
        self.circles = np.uint16(np.around(self.circles));

    def countCircles(self):
        return len(self.circles[0,:]);

    def drawCircles(self):
        for i in self.circles[0,:]:
           cv2.circle(self.img,(i[0],i[1]),i[2],(255,255,0),2)
           cv2.circle(self.img,(i[0],i[1]),2,(255,255,0),3)
