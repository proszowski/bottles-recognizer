import cv2
from matplotlib import pyplot as plt

class ImageDisplayer:
    def __init__(self):
        pass;

    def display(self, img):
        plt.imshow(img), plt.xticks([]), plt.yticks([]);
        plt.show();

    def display(self, img, title):
        plt.imshow(img), plt.xticks([]), plt.yticks([]), plt.title(title);
        plt.show();

    def displaySeparately(self, originalImages, expected, changedImages, actual):
        size = len(originalImages)

        for x in range(0, size):
            img = changedImages[x];
            orig = originalImages[x];
            plt.subplot(2,1,1), plt.imshow(orig, cmap='gray'), plt.title(expected[x]), plt.xticks([]), plt.yticks([]);
            plt.subplot(2,1,2), plt.imshow(img), plt.title(actual[x]), plt.yticks([]), plt.xticks([]);
            plt.show();

    def displaySeparatelyBothInGrayScale(self, originalImages, changedImages):
        size = len(originalImages)

        for x in range(0, size):
            img = changedImages[x];
            orig = originalImages[x];
            plt.subplot(2,1,1), plt.imshow(orig, cmap='gray');
            plt.subplot(2,1,2), plt.imshow(img, cmap='gray');
            plt.show();

