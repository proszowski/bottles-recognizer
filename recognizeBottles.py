from bottlesrecognizer import BottlesRecognizer
from imagedisplayer import ImageDisplayer
from tkinter import *
import sys

def center(win):
    win.update_idletasks()
    width = win.winfo_width()
    frm_width = win.winfo_rootx() - win.winfo_x()
    win_width = width + 2 * frm_width
    height = win.winfo_height()
    titlebar_height = win.winfo_rooty() - win.winfo_y()
    win_height = height + titlebar_height + frm_width
    x = win.winfo_screenwidth() // 2 - win_width // 2
    y = win.winfo_screenheight() // 2 - win_height // 2
    win.geometry('{}x{}+{}+{}'.format(width, height, x, y))
    win.deiconify()

def calculateBottles(pathToImage):
    bottlesRecognizer = BottlesRecognizer(pathToImage);
    numberOfBottles = bottlesRecognizer.countBotttles();
    title = "Number of bottles: " + str(numberOfBottles);
    img = bottlesRecognizer.getImageWithMarkedBottles();
    imageDisplayer = ImageDisplayer();
    imageDisplayer.display(img, title);

def main():
    root = Tk();
    root.geometry("400x80");
    root.resizable(0, 0);
    root.title("Bottles recognizer");
    center(root);
    topFrame = Frame(root);
    e = Entry(topFrame, bd=5);
    e.pack(side=RIGHT);
    l = Label(topFrame, text="Path to image:");
    l.pack(side=LEFT);
    topFrame.pack();
    bottomFrame = Frame(root);
    okButton = Button(bottomFrame, text="CALCULATE BOTTLES", command = lambda : calculateBottles(e.get()));
    okButton.pack(side=LEFT);
    closeButton = Button(bottomFrame, text="CLOSE", command = lambda : sys.exit());
    closeButton.pack(side=RIGHT);
    bottomFrame.pack();
    root.mainloop();

if __name__ == "__main__":
    main();
