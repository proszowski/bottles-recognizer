#What is it?
Program which counts number of bottles inside the box.

#How to run?
python3 recognizeBottles.py or python3 bottlesrecognizertest.py to see results for set of images that actually is in repo.

#How does it work?
First it uses gaussian blur to remove shapes which could be wrongly recognized as circles and then it uses Hough Transformation to find circles with fixed radius on the image (radius isn't configurable, it fits for images inside folder img but not necessarily for other images).

#Requirements:
* python3
* openCV
* tkinter
