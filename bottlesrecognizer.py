import cv2
from houghcircletransformation import HoughCircleTransformation
from imageenhancer import ImageEnhancer

class BottlesRecognizer:
    def __init__(self, path):
        self.img = cv2.imread(path, 0);
        self._enhanceImage();
        self.hct = HoughCircleTransformation(self.img);

    def _enhanceImage(self):
        imageEnhancer = ImageEnhancer();
        self.img = imageEnhancer.enhanceImage(self.img);

    def countBotttles(self):
        return self.hct.countCircles();

    def getImageWithMarkedBottles(self):
        self.hct.drawCircles();
        return self.img;
